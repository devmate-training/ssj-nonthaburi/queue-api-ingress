'use strict';

/** @type {import('fluent-json-schema').default} */
const S = require('fluent-json-schema');

const queueModel = require('../models/queue');
const { DateTime } = require('luxon');

module.exports = async function (fastify, opts) {
	fastify.post(
		'/viewer',
		{
			schema: {
				body: S.object()
					.prop(
						'hn',
						S.string()
							.pattern(/^[0-9]{7}$/)
							.required()
					)
					.prop('departmentId', S.string().required()),
			},
		},
		async function (request, reply) {
			const { hn, departmentId } = request.body;

			try {
				const info = await queueModel.getInfo(fastify.db, hn, departmentId);
				if (!info) {
					return reply
						.status(400)
						.send({ ok: false, message: 'Visit not found' });
				}

				// Get queue
				const queue = await queueModel.getQueueCaller(fastify.db, departmentId);
				if (!queue) {
					return reply
						.status(400)
						.send({ ok: false, message: 'Queue not found' });
				}

				return reply.status(200).send({ ok: true, info, queue });
			} catch (error) {
				console.log(error);
				return reply
					.status(500)
					.send({ ok: false, message: 'Internal server error' });
			}
		}
	);
};
