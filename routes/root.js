'use strict';

module.exports = async function (fastify, opts) {
	fastify.get('/', async function (request, reply) {
		return { root: true };
	});
	// Start job
	fastify.get(
		'/genkey',
		{
			onRequest: fastify.basicAuth,
		},
		async function (request, reply) {
			const token = await fastify.jwt.sign({
				sub: 'hosxxxxx',
				name: 'Satit Rianpit',
			});

			return { token };
		}
	);
};
