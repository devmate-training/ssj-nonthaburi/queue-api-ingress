'use strict';

const { DateTime } = require('luxon');

module.exports = async function (fastify, opts) {
	fastify.post('/', async function (request, reply) {
		const data = request.body;
		try {
			const db = fastify.db;
			const _data = data.map((item) => {
				return {
					...item,
					created_at: DateTime.now().toSQLDate(),
				};
			});

			for (const item of _data) {
				await db('queue_caller')
					.insert(item)
					.onConflict('department_id')
					.merge(['queue_no', 'updated_at']);
			}

			return reply.status(200).send({ ok: true });
		} catch (error) {
			console.log(error);
			return reply.status(500).send({ ok: false });
		}
	});

	fastify.post('/visit', async function (request, reply) {
		const data = request.body;

		try {
			const _data = data.map((item) => {
				return {
					...item,
					vstdate: DateTime.fromISO(item.vstdate).toSQLDate(),
				};
			});
			const db = fastify.db;
			await db('patient_visit')
				.insert(_data)
				.onConflict(['hn', 'vn'])
				.merge(['queue_no', 'vstdate', 'department_name', 'first_name', 'sex']);
			return reply.status(200).send({ ok: true });
		} catch (error) {
			console.log(error);
			return reply.status(500).send({ ok: false });
		}
	});
};
