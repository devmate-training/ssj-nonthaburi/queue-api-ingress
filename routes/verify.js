'use strict';

/** @type {import('fluent-json-schema').default} */
const S = require('fluent-json-schema');

const queueModel = require('../models/queue');
const { DateTime } = require('luxon');

module.exports = async function (fastify, opts) {
	fastify.post(
		'/verify',
		{
			schema: {
				body: S.object().prop(
					'hn',
					S.string()
						.pattern(/^[0-9]{7}$/)
						.required()
				),
			},
		},
		async function (request, reply) {
			const { hn } = request.body;
			const vstdate = DateTime.now().toSQLDate(); // yyyy-MM-dd
			try {
				const results = await queueModel.verify(fastify.db, hn, vstdate);
				if (results.length === 0) {
					return reply
						.status(400)
						.send({ ok: false, message: 'Visit not found' });
				}

				return reply.status(200).send({ ok: true, results });
			} catch (error) {
				console.log(error);
				return reply
					.status(500)
					.send({ ok: false, message: 'Internal server error' });
			}
		}
	);
};
