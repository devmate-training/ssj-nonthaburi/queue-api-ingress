module.exports = {
	/**
	 * @param {import('knex').Knex} db Knex instance
	 * @param {string} hn Patient hospital number
	 * @param {string} vstdate Visit date
	 */
	verify(db, hn, vstdate) {
		return db('patient_visit').where('hn', hn).where('vstdate', vstdate);
	},
	/**
	 * Get queue no
	 * @param {import('knex').Knex} db Knex instance
	 * @param {string} departmentId Department ID
	 */
	getQueueCaller(db, departmentId) {
		return db('queue_caller').where('department_id', departmentId).first();
	},

	/**
	 * Get Visit info
	 * @param {import('knex').Knex} db Knex instance
	 * @param {string} hn Patient HN
	 * @param {string} departmentId Department ID
	 */
	getInfo(db, hn, departmentId) {
		return db('patient_visit')
			.where('hn', hn)
			.where('department_id', departmentId)
			.first();
	},
};
