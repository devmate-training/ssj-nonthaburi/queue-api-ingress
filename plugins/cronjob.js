'use strict';

const fp = require('fastify-plugin');

module.exports = fp(async function (fastify, opts) {
	fastify.register(require('fastify-cron'), {
		jobs: [
			{
				name: 'clearVisit',
				cronTime: '0 1 * * *',
				onTick: async (server) => {
					console.log('Clear visit');
					const db = fastify.db;
					try {
						const lastDate = DateTime.now().minus({ days: 1 }).toSQLDate();

						await db('patient_visit').where('vstdate', lastDate).del();
					} catch (error) {
						console.error(error);
					}
				},
				startWhenReady: true,
			},
			{
				name: 'clearQueue',
				cronTime: '0 1 * * *',
				onTick: async (server) => {
					console.log('Clear Queue');
					const db = fastify.db;
					try {
						const lastDate = DateTime.now().minus({ days: 1 }).toSQLDate();

						await db('queue_caller').where('created_at', lastDate).del();
					} catch (error) {
						console.error(error);
					}
				},
				startWhenReady: true,
			},
		],
	});
});
