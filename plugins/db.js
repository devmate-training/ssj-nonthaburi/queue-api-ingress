'use strict';

const fp = require('fastify-plugin');
const Knex = require('knex');

module.exports = fp(async function (fastify) {
	if (!fastify.db) {
		const knex = Knex({
			client: 'mysql2',
			connection: {
				host: process.env.DB_HOST || 'localhost',
				port: process.env.DB_PORT ? Number(process.env.DB_PORT) : 3306,
				database: process.env.DB_NAME || 'test',
				user: process.env.DB_USER || 'root',
				password: process.env.DB_PASSWORD || '123456',
			},
			debug: process.env.NODE_ENV === 'development',
			pool: {
				min: 2,
				max: 10,
				afterCreate: (conn, done) => {
					conn.query('SET NAMES utf8mb4', (err) => {
						done(err, conn);
					});
				},
			},
		});

		fastify.decorate('db', knex);

		fastify.addHook('onClose', (fastify, done) => {
			if (fastify.knex === knex) {
				fastify.knex.destroy(done);
			}
		});
	}
});
